use std::collections::HashMap;

use serde::{self, Deserialize, Serialize};
use url::Url;
use crate::matrix::event::Event;
use crate::matrix::filter::FilterDefinition;
use crate::matrix::content::Content;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum SyncFilter {
    #[serde(with = "filter_def_serde")]
    FilterDefinition(FilterDefinition),
    FilterId(String),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SyncResponse {
    pub next_batch: String,
    pub rooms: RoomsSyncResponse,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RoomsSyncResponse {
    pub invite: HashMap<String, InvitedRoomSyncResponse>,
    pub join: HashMap<String, JoinedRoomSyncResponse>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct InvitedRoomSyncResponse {
    pub invite_state: InviteState,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct JoinedRoomSyncResponse {
    pub timeline: Timeline,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Timeline {
    pub events: Vec<Event>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct InviteState {
    pub events: Vec<StrippedState>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct StrippedState {
    content: Content,
    state_key: String,
    #[serde(rename = "type")]
    event_type: String,
    sender: String,
}

mod filter_def_serde {
    use serde::{de::Error as _, ser::Error as _, Deserialize, Deserializer, Serializer};

    use crate::matrix::filter::FilterDefinition;

    pub fn serialize<S>(filter_def: &FilterDefinition, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        let string = serde_json::to_string(filter_def).map_err(S::Error::custom)?;
        serializer.serialize_str(&string)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<FilterDefinition, D::Error>
        where
            D: Deserializer<'de>,
    {
        let filter_str = <&str>::deserialize(deserializer)?;
        serde_json::from_str(filter_str).map_err(D::Error::custom)
    }
}
