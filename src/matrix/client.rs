use std::error::Error;
use std::io::ErrorKind;
use std::mem;
use std::time::{SystemTime, UNIX_EPOCH};

use futures::{Async, try_ready};
use futures::future::Future;
use futures::Stream;
use futures::stream;
use futures::stream::Concat2;
use image::DynamicImage;
use reqwest::r#async::{Client, ClientBuilder};
use reqwest::r#async::Decoder;
use serde_json::json;
use tokio::await;
use url::Url;

use crate::matrix::content::{MessageType, NoticeMessageEventContent, TextMessageEventContent};
use crate::matrix::event::Event;
use crate::matrix::sync::{SyncFilter, SyncResponse};

pub struct MatrixClient {
    server_address: Url,
    access_token: String,

    id: u128,
    count: u32,
    client: Client,
}

impl MatrixClient {
    pub fn new(server_address: Url, access_token: String) -> MatrixClient {
        let client = ClientBuilder::new().build().unwrap();

        MatrixClient {
            server_address,
            access_token,

            id: SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis(),
            count: 0,
            client,
        }
    }

    pub fn sync_stream(&self, since: Option<String>, filter: Option<SyncFilter>) -> impl Stream<Item=SyncResponse, Error=reqwest::Error> {
        let client = self.client.clone();

        let mut api_url = self.server_address.clone();
        api_url.set_path("/_matrix/client/r0/sync");

        let token = self.access_token.clone();

        stream::unfold(since, move |since| {
            let mut queries: Vec<(&str, String)> = vec![("timeout", 30000.to_string())];

            if let Some(s) = since.clone() {
                queries.push(("since", s));
            }

            if let Some(f) = filter.clone() {
                if let SyncFilter::FilterDefinition(definition) = f {
                    queries.push(("filter", serde_json::to_string(&definition).unwrap()));
                } else if let SyncFilter::FilterId(id) = f {
                    queries.push(("filter", id));
                }
            }

            Some(
                client.get(api_url.clone()).query(&queries.clone()).bearer_auth(token.clone()).send().and_then(|mut raw_response| {
                    raw_response.json::<SyncResponse>()
                }).map(|response| {
                    let next_batch_clone = response.next_batch.clone();
                    (response, Some(next_batch_clone))
                }),
            )
        })
    }

    pub async fn join_room(&self, room_id: String) {
        let mut api_url = self.server_address.clone();
        api_url.set_path(format!("/_matrix/client/r0/rooms/{}/join", room_id).as_str());

        await!(self.client.post(api_url).bearer_auth(self.access_token.clone()).send());
    }

    pub async fn send_text(&mut self, room_id: String, text: String) {
        let mut api_url = self.server_address.clone();
        api_url.set_path(format!("/_matrix/client/r0/rooms/{}/send/{}/{}", room_id, "m.room.message", self.session_id()).as_str());

        await!(self.client.put(api_url).bearer_auth(self.access_token.clone()).body(serde_json::to_string(&TextMessageEventContent {
            msgtype: MessageType::Text,
            body: text,
        }).unwrap()).send());
    }

    pub async fn send_notice(&mut self, room_id: String, text: String) {
        let mut api_url = self.server_address.clone();
        api_url.set_path(format!("/_matrix/client/r0/rooms/{}/send/{}/{}", room_id, "m.room.message", self.session_id()).as_str());

        await!(self.client.put(api_url).bearer_auth(self.access_token.clone()).body(serde_json::to_string(&NoticeMessageEventContent {
            msgtype: MessageType::Notice,
            body: text,
        }).unwrap()).send());
    }

    pub async fn fetch_image(&self, image_url: Url) -> Result<DynamicImage, Box<dyn Error>> {
        let mut api_url = self.server_address.clone();
        api_url.set_path(format!("/_matrix/media/r0/download/{}/{}", image_url.host_str().unwrap(), image_url.path().to_string().trim_start_matches("/")).as_str());

        let chunk = await!(self.client.get(api_url).send().and_then(|mut resp| {
            let body = mem::replace(resp.body_mut(), Decoder::empty());
            body.concat2()
        })
        )?;

        let img = image::load_from_memory(&(chunk))?;

        Ok(img)
    }

    fn session_id(&mut self) -> String {
        self.count += 1;
        format!("{}_{}", self.id, self.count)
    }
}