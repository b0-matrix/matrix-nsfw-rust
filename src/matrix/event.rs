use serde::{Deserialize, Deserializer, Serialize, de};
use serde_json::Value;

use crate::matrix::content::{Content, deserialize_content};

#[derive(Serialize, Clone, Debug)]
pub struct Event {
    #[serde(rename = "type")]
    pub event_type: String,
    /// Event content.
    pub content: Content,
}

impl<'de> Deserialize<'de> for Event {
    fn deserialize<D>(d: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        let v = Value::deserialize(d)?;
        let typ = v.get("type").ok_or(de::Error::custom("No event type field"))?;
        let typ = match *typ {
            Value::String(ref s) => s as &str,
            _ => Err(de::Error::custom("Event type is not a string"))?
        };
        let c = v.get("content").ok_or(de::Error::custom("No content field"))?;
        let content = deserialize_content(typ, c.clone());
        Ok(Event {
            event_type: typ.into(),
            content,
        })
    }
}
