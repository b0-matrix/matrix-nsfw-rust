use std::error::Error;

use image::{DynamicImage, FilterType};
use tensorflow as tf;

const VGG_VAL: [f32; 3] = [104.0, 117.0, 123.0];

pub struct Detector {
    session: tf::Session,
    input: tf::Operation,
    output: tf::Operation,
}

impl Detector {
    pub fn new(model_path: String) -> Result<Detector, Box<dyn Error>> {
        let mut graph = tf::Graph::new();
        let session = tf::Session::from_saved_model(&tf::SessionOptions::new(),
                                                    &["serve"],
                                                    &mut graph,
                                                    model_path)?;
        let input = graph.operation_by_name_required("input")?;
        let output = graph.operation_by_name_required("predictions")?;

        let detector = Detector {
            session,
            input,
            output,
        };

        Ok(detector)
    }

    // Returns NSFW value.
    pub fn detect(&self, input_image: DynamicImage) -> Result<f32, Box<dyn Error>> {
        let tensor: tf::Tensor<f32> = {
            let buffer: Vec<u8> = input_image.resize_exact(224, 224, FilterType::Gaussian).to_bgr().to_vec();
            let buffer_f32: Vec<f32> = buffer.iter().enumerate().map(|(index, &val)| (val as f32) - VGG_VAL[index % 3]).collect();

            tf::Tensor::new(&[1, 224, 224, 3]).with_values(&buffer_f32).unwrap()
        };

        let mut step = tf::SessionRunArgs::new();
        step.add_feed(&self.input, 0, &tensor);
        let fetch_token = step.request_fetch(&self.output, 0);

        self.session.run(&mut step)?;

        let res: tf::Tensor<f32> = step.fetch(fetch_token)?;

        Ok(res[1])
    }
}