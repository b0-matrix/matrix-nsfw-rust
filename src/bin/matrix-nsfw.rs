#![feature(await_macro, async_await, futures_api)]

extern crate matrix_nsfw;

use std::convert::TryFrom;
use std::error::Error;
use std::time::{Duration, Instant};

use clap::{App, Arg, SubCommand};
use futures::stream::Stream;
use serde_json::json;
use tokio::await;
use tokio::timer;
use tokio::timer::Delay;
use tokio_async_await::stream::StreamExt;
use url::Url;

use matrix_nsfw::matrix::client::MatrixClient;
use matrix_nsfw::matrix::content::Content::RoomMessage;
use matrix_nsfw::matrix::content::MessageEventContent::Image;
use matrix_nsfw::matrix::filter;
use matrix_nsfw::matrix::sync::SyncFilter;

async fn run(server_address: String, access_token: String) -> Result<(), Box<dyn Error>> {
    let mut _client = MatrixClient::new(Url::parse(server_address.as_str()).unwrap(), access_token.to_string());
    let _detector = matrix_nsfw::detector::Detector::new("./asset/model/".to_string())?;

    let filter_all = filter::Filter {
        not_types: vec!["*".to_owned()],
        limit: None,
        senders: Vec::new(),
        types: Vec::new(),
        not_senders: Vec::new(),
    };
    let filter_all_events = filter::RoomEventFilter {
        not_types: vec!["*".to_owned()],
        not_rooms: Vec::new(),
        limit: None,
        rooms: Vec::new(),
        not_senders: Vec::new(),
        senders: Vec::new(),
        types: Vec::new(),
    };
    let filter_room_events = filter::RoomEventFilter {
        not_types: Vec::new(),
        not_rooms: Vec::new(),
        limit: None,
        rooms: Vec::new(),
        not_senders: Vec::new(),
        senders: Vec::new(),
        types: vec!["m.room.message".to_owned()],
    };
    let room_filter = filter::RoomFilter {
        include_leave: Some(true),
        account_data: Some(filter_all_events.clone()),
        timeline: Some(filter_room_events.clone()),
        ephemeral: Some(filter_all_events.clone()),
        state: Some(filter_all_events),
        not_rooms: Vec::new(),
        rooms: Vec::new(),
    };
    let filter_definition = filter::FilterDefinition {
        event_fields: Vec::new(),
        event_format: None,
        account_data: Some(filter_all.clone()),
        room: Some(room_filter.clone()),
        presence: Some(filter_all.clone()),
    };

    let mut stream = _client.sync_stream(None, Some(SyncFilter::FilterDefinition(filter_definition)));

    if let Some(resp) = await!(stream.next()) {
        for (room_id, _) in resp?.rooms.invite {
            println!("Invited to {}", room_id.clone());
            await!(_client.join_room(room_id));
        }
    }

    println!("Client initialized.");

    while let Some(resp) = await!(stream.next()) {
        let sync_response = resp?;

        for (room_id, _) in sync_response.rooms.invite {
            println!("Invited to {}", room_id.clone());
            await!(_client.join_room(room_id));
        }

        for (room_id, room_sync_response) in sync_response.rooms.join {
            for event in room_sync_response.timeline.events {
                if let RoomMessage(content) = event.content {
                    if let Image(image_content) = content {
                        let img = await!(_client.fetch_image(Url::parse(image_content.url.as_str()).unwrap()))?;

                        let nsfw_value = _detector.detect(img)?;

                        await!(_client.send_notice(room_id.clone(), serde_json::to_string(&json!({
                        "is_nsfw": nsfw_value > 0.3,
                        "nsfw_value": nsfw_value,
                        })).unwrap()));
                    }
                }
            }
        }
    }

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("My Super Program")
        .version("0.0")
        .author("Black Hat <bhat@encom.eu.org>")
        .about("Detects NSFW!")
        .arg(Arg::with_name("addr")
            .long("addr")
            .value_name("ADDRESS")
            .help("Sets server address")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("userid")
            .long("userid")
            .value_name("ID")
            .help("Sets user id")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("token")
            .long("token")
            .value_name("TOKEN")
            .help("Set access token")
            .required(true)
            .takes_value(true))
        .get_matches();

    let server_address = matches.value_of("addr").unwrap().to_string();
    let access_token = matches.value_of("token").unwrap().to_string();
    let user_id = matches.value_of("userid").unwrap().to_string();

    tokio::run_async(async move {
        match await!(run(server_address, access_token)) {
            Ok(_) => println!("done."),
            Err(e) => eprintln!("echo client failed; error = {:?}", e),
        }
    });

    Ok(())
}
